<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Plugin;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Tigren\Rules\Model\ResourceModel\GroupRule\CollectionFactory as GroupRuleCollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\Session;

/**
 * Class MagentoCatalogModelProduct
 * @package Tigren\Rules\Plugin
 */
class MagentoCatalogModelProduct
{
    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var GroupRuleCollectionFactory
     */
    protected $groupRuleCollection;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * Discount constructor.
     * @param Session $customerSession
     * @param CollectionFactory $productCollectionFactory
     * @param GroupRuleCollectionFactory $collection
     * @throws \Magento\Framework\Exception\SessionException
     */
    public function __construct(
        Session $customerSession,
        CollectionFactory $productCollectionFactory,
        GroupRuleCollectionFactory $collection
    ) {
        $this->_customerSession = $customerSession->start();
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->groupRuleCollection = $collection;
    }

    public function aroundIsSalable()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @param Product $product
     * @param $result
     * @return float|int|mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterGetSpecialPrice(Product $product, $result)
    {
        $customerGroupId = $this->_customerSession->getCustomerGroupId();
        $rules = $this->groupRuleCollection
            ->create()
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->setOrder('priority', 'asc');
        if (isset($rules)) {
            foreach ($rules as $rule) {
                $groupIds = explode(',', $rule->getCustomerGroupIds());
                if (in_array($customerGroupId, $groupIds)) {
                    $productIds = explode(',', $rule->getProductIds());
                    $discount_amount = (int)$rule->getDiscountAmount();
                    break;
                }
            }
        }
        if (isset($productIds)) {
            if (in_array($product->getId(), $productIds)) {
                $result = $product->getPrice() - ($product->getPrice() * $discount_amount / 100);
            }
        }
        return $result;
    }
}
