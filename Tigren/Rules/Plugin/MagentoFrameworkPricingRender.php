<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Plugin;

use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\Render;
use Tigren\Rules\Model\ResourceModel\GroupRule\Collection;

/**
 * Class MagentoFrameworkPricingRender
 * @package Tigren\Rules\Plugin
 */
class MagentoFrameworkPricingRender
{
    /**
     * @var Collection
     */
    protected $groupRuleCollection;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * ChangePrice constructor.
     * @param Collection $collection
     * @param Session $customerSession
     * @throws \Magento\Framework\Exception\SessionException
     */
    public function __construct(
        Collection $collection,
        Session $customerSession
    ) {
        $this->_customerSession = $customerSession->start();
        $this->groupRuleCollection = $collection;
    }

    /**
     * @param Render $subject
     * @param $result
     * @return string
     */
    public function afterRender(Render $subject, $result)
    {
        if ($this->_customerSession->isLoggedIn()) {
            return $result;
        } else {
            $result = 'Please login to see price .';
            return $result;
        }
    }
}
