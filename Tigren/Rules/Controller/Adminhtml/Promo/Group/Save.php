<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Controller\Adminhtml\Promo\Group;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Tigren\Rules\Model\GroupRule;

/**
 * Class Save
 * @package Tigren\Rules\Controller\Adminhtml\Promo\Group
 */
class Save extends \Magento\Backend\App\Action
{

    /**
     * @var GroupRule
     */
    protected GroupRule $rulesModel;

    /**
     * @var Session
     */
    protected Session $adminsession;

    /**
     * @param Action\Context $context
     * @param GroupRule $rulesModel
     * @param Session $adminsession
     */
    public function __construct(
        Action\Context $context,
        GroupRule $rulesModel,
        Session $adminsession
    ) {
        parent::__construct($context);
        $this->rulesModel = $rulesModel;
        $this->adminsession = $adminsession;
    }

    /**
     * Save rule record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $data['store_ids'] = implode(",", $data['store_ids']);
        $data['customer_group_ids'] = implode(",", $data['customer_group_ids']);
        $arr_products = $data['product_ids'];
        $product_ids = [];
        foreach ($arr_products as $key => $value) {
            $product_ids[] = $value['entity_id'];
        }
        $data['product_ids'] = implode(",", $product_ids);

        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $rule_id = $this->getRequest()->getParam('rule_id');
            if ($rule_id) {
                $this->rulesModel->load($rule_id);
            }

            $this->rulesModel->setData($data);

            try {
                $this->rulesModel->save();
                $this->messageManager->addSuccess(__('The data has been saved.'));
                $this->adminsession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    if ($this->getRequest()->getParam('back') == 'add') {
                        return $resultRedirect->setPath('*/*/add');
                    } else {
                        return $resultRedirect->setPath('*/*/edit',
                            ['rule_id' => $this->rulesModel->getRuleId(), '_current' => true]);
                    }
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['rule_id' => $this->getRequest()->getParam('rule_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
