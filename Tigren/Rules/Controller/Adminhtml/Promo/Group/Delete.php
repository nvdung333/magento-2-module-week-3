<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Controller\Adminhtml\Promo\Group;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Tigren\Rules\Model\GroupRule;

/**
 * Class Delete
 * @package Tigren\Rules\Controller\Adminhtml\Promo\Group
 */
class Delete extends Action
{
    protected $modelGroupRule;

    public function __construct(
        Context $context,
        GroupRule $groupRuleFactory
    ) {
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_Rules::grouprule_delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('rule_id');
        if (!($rule = $this->_objectManager->create(GroupRule::class)->load($id))) {
            $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }
        try {
            $rule->delete();
            $this->messageManager->addSuccess(__('Your rule has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addError(__('Error while trying to delete rule: '));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }
}
