<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Model\ResourceModel\GroupRule;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Tigren\Rules\Model\GroupRule as GroupRuleModel;
use Tigren\Rules\Model\ResourceModel\GroupRule as GroupRuleResourceModel;

/**
 * Class Collection
 * @package Tigren\Rules\Model\ResourceModel\GroupRule
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(GroupRuleModel::class, GroupRuleResourceModel::class);
    }
}
