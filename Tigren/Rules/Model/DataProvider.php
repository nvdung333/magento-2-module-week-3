<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Model;

use Tigren\Rules\Model\ResourceModel\GroupRule\CollectionFactory;

/**
 * Class DataProvider
 * @package Tigren\Rules\Model
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var array
     */
    protected $loadedData;

    // @codingStandardsIgnoreStart
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $groupRuleCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $groupRuleCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    // @codingStandardsIgnoreEnd

    public function getData()
    {

        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $rule) {
            $this->loadedData[$rule->getRuleId()] = $rule->getData();
        }
        return $this->loadedData;
    }
}
