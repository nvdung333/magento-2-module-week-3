<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Model;

use Magento\Framework\Model\AbstractModel;
use Tigren\Rules\Model\ResourceModel\GroupRule as GroupRuleResourceModel;

/**
 * Class GroupRule
 * @package Tigren\Rules\Model
 */
class GroupRule extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(GroupRuleResourceModel::class);
    }
}
