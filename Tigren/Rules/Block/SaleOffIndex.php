<?php
/*
 * @author  Tigren Solutions <info@tigren.com>
 * @copyright Copyright (c) 2021 Tigren Solutions <https://www.tigren.com>. All rights reserved.
 * @license  Open Software License ("OSL") v. 3.0
 */

namespace Tigren\Rules\Block;

use Tigren\Rules\Model\ResourceModel\GroupRule\CollectionFactory as RuleCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;


/**
 * Class SaleOffIndex
 * @package Tigren\Rules\Block
 */
class SaleOffIndex extends Template
{
    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var RuleCollectionFactory
     */
    protected $ruleCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * Index constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param Session $customerSession
     * @param Context $context
     * @param RuleCollectionFactory $ruleCollectionFactory
     * @throws SessionException
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        Session $customerSession,
        Context $context,
        RuleCollectionFactory $ruleCollectionFactory
    )
    {
        parent::__construct($context);
        $this->ruleCollectionFactory = $ruleCollectionFactory;
        $this->_customerSession = $customerSession->start();
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @return Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProducts()
    {
        $products = "";
        if ($this->getRule() != null) {
            $products = $this->productCollectionFactory
                ->create()
                ->addAttributeToSelect('*')
                ->addIdFilter(['in' => explode(',', $this->getRule()->getProductIds())]);
        }
        return $products;
    }

    public function getRule()
    {
        $idCustomerSession = $this->_customerSession->getCustomerGroupId();
        $rules = $this->ruleCollectionFactory
            ->create()
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->setOrder('priority', 'asc');
        foreach ($rules as $rule) {
            $arrGroupId =  explode(',',$rule->getCustomerGroupIds());
            if(in_array($idCustomerSession,$arrGroupId)){
                return $rule;
            }
        }
    }
}
